package br.edu.ifpb.pweb2.demosb.modelo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Aluno {
	
	private Integer id;
	private String nome;
	private String matricula;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataAniversario;
	private Integer cre;
	private boolean ativo;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Date getDataAniversario() {
		return dataAniversario;
	}

	public void setDataAniversario(Date dataAniversario) {
		this.dataAniversario = dataAniversario;
	}

	public Integer getCre() {
		return cre;
	}

	public void setCre(Integer cre) {
		this.cre = cre;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
