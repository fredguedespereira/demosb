package br.edu.ifpb.pweb2.demosb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.edu.ifpb.pweb2.demosb.modelo.Usuario;

@Controller
@RequestMapping("login")
public class LoginController {
	
	@RequestMapping
	public String getFormLogin(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "/login/form-login";
	}
	
	@RequestMapping(method={RequestMethod.POST})
	public String validaLogin(Usuario usuario, Model modelo) {
		String proxPagina = null;
		if (usuario.getLogin().equalsIgnoreCase("fred") && usuario.getSenha().equals("123")) {
			modelo.addAttribute("usuario", usuario.getLogin());
			proxPagina = "/index";
		} else {
			modelo.addAttribute("mensagem", "Login e/ou senha inválidos!");
			modelo.addAttribute("usuario", usuario);
			proxPagina = "/login/form-login";
		}
		return proxPagina;
	}
	
	@RequestMapping("out")
	public String logout() {
		System.out.println("Fez logout...");
		return "index";
	}
	

}
