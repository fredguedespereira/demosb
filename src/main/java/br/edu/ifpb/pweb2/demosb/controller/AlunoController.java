package br.edu.ifpb.pweb2.demosb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.edu.ifpb.pweb2.demosb.modelo.Aluno;
import br.edu.ifpb.pweb2.demosb.repository.AlunoRepository;

@Controller
@RequestMapping("/alunos")
public class AlunoController {
	
	@RequestMapping("/form")
	public String getFormAluno(Model model) {
		model.addAttribute("aluno", new Aluno());
		return "/alunos/form-aluno";
	}
	
	@RequestMapping(method = {RequestMethod.POST}) //P-R-G
	public String cadastreAluno(Aluno aluno, Model model) {
		if (aluno.getId() == null) {
			AlunoRepository.insert(aluno);
		} else {
			AlunoRepository.update(aluno);
		}
		model.addAttribute("alunos", AlunoRepository.findAll());
		return "redirect:/alunos";
	}
	
	@RequestMapping(method = {RequestMethod.GET})
	public String listeAlunosMatriculados(Model model) {
		model.addAttribute("alunos", AlunoRepository.findAll());
		return "/alunos/list-aluno";
	}
	
	@RequestMapping(value="/{id}", method = {RequestMethod.GET})
	public String editeAluno(@PathVariable("id") Integer id, Model model) {
		Aluno aluno = AlunoRepository.findById(id);
		model.addAttribute("aluno", aluno);
		return "/alunos/form-aluno";
	}
	
	
	
	
	

}
