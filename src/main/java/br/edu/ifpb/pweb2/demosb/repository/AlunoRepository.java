package br.edu.ifpb.pweb2.demosb.repository;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import br.edu.ifpb.pweb2.demosb.modelo.Aluno;


public class AlunoRepository {
	private static Map<Integer, Aluno> repositorio = new HashMap<Integer, Aluno>();
	
	public static Aluno findById(Integer id) {
		return repositorio.get(id);
	}
	
	public static void insert(Aluno aluno) {
		int id = getMaxId();
		aluno.setId(id);
		repositorio.put(id, aluno);
	}
	
	public static void update(Aluno aluno) {
		repositorio.put(aluno.getId(), aluno);
	}
	
	public static List<Aluno> findAll() {
		List<Aluno> alunos = repositorio
				.values().
				stream().
				collect(Collectors.toList());
		return alunos;
	}
	
	private static Integer getMaxId() {
		List<Aluno> alunos = findAll();
		if (alunos == null || alunos.isEmpty()) return 1;
		Aluno contaMaxId = alunos
				.stream()
				.max(Comparator.comparing(Aluno::getId))
				.orElseThrow(NoSuchElementException::new);
		return contaMaxId.getId() == null ? 1 : contaMaxId.getId() + 1;
	}
}